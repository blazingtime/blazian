# blazian

## Description
A personal Debian 12 (Bookworm) live-build with GNOME, as well as my configuration.

## Important notes:
- Project is heavily WIP. This is not functional at all for now.
- Project is for my personal use. Sometimes I have stupid ways of doing things, which sometimes lead to unexpected behaviour.

## Features
- Minimal base system with GNOME as DE. "gnome-core" package is used. No debian nonsence like 2048 game.
- Relies heavily on flatpak. Use rock-stable debian base system with latest and greatest secure containerized linux apps.
- Apt does not install recommends by default.

## Installation
Download iso from releases and flash it onto usb-thumb.
You can build your own iso by cloning this repo installing "live-build" and running "lb config" and "sudo lb build".
Initial config uses following command:
"lb config --apt-recommends false --archive-areas "main contrib non-free non-free-firmware" --backports true --compression xz --debian-installer live --debian-installer-distribution bookworm --debian-installer-gui false --debootstrap-options "--variant=minbase" -d bookworm --image-name blazian --security true --updates true --verbose"

## Usage
It is debian lol.

## Support
Check Issues.

## Roadmap
- add anydesk repo - done? test
- add kxstudio repo - done, test
- add nala - done, test
- check dwm dependancies - done, test
- add dwm complie script, send source to ~/.config/dwm
- add st - done, test
- add custom dotfiles
- add ~/bin folder and add to $PATH for custom scripts - included in dotfiles 
- add sysvinit branch, test usability
- add theming: gtk2-4, qt4-6
- actually configure st
- add system configuration to /etc
- add fonts: flexi IBM false
- add wallpaper
- maybe add dwm-flexipatch?
- configure apt
- edit config/includes.chroot/etc/skel/.gtkrc-2.0
- add lxappearence, arandr, picom
- remove screen tearing, enable vsync
- add bleachbit, disk analyser
- add calcurse
- add flacon
- add flameshot, configure
- add galculator (or terminal calculator?)
- add KVM
- add laptop support
- add lutris, manguhud, goverlay
- add mpv
- add neofetch
- add pcmanfm, add automount flash
- replase qbittorrent with transmission, or even better with cli-daemon
- add pdf viewer+editor
- add qt5ct, lxappearance
- add qtile and openbox as an option
- add remmina + rdp
- add scanner app
- preconfigured whonix VM?
- add xarchiver
- add pacstall installation hook
- ensure easy to setup wifi
- bluetooth support
- add kdenlive
- add krita
- add custom kernel
- add flatpak apps

## Contributing
Use Merge Requests.
