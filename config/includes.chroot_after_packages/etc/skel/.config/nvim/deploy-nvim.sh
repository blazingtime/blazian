#!/bin/bash
sudo apt install curl git exuberant-ctags
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
sudo curl -sL install-node.vercel.app/lts | sudo bash
curl --compressed -o- -L https://yarnpkg.com/install.sh | bash


echo ""
echo "neovim deployed, now execute following inside nvim:"
echo ":PlugInstall"
echo ":CocInstall coc-clangd coc-clang-format-style-options coc-cmake coc-css "
echo ":CocInstall coc-cssmodules coc-diagnostic coc-dash-complete coc-git coc-highlight"
echo ":CocInstall coc-html coc-htmlhint coc-html-css-support coc-java coc-json coc-lua"
echo ":CocInstall coc-markdownlint coc-markdown-preview-enhanced coc-perl coc-powershell"
echo ":CocInstall  coc-python coc-sh coc-rust-analyzer coc-spell-checker coc-sql coc-xml coc-yaml"
